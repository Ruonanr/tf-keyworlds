## 使用Tensorflow识别语音关键词   
本项目使用Tensorflow实现某些关键词识别，可以用于移动设备的语音控制，识别包括[yes，no,up,down,on,off]等，本项目最终可以在Android上运行。  

## 运行代码
[完整项目](https://gitlab.com/Kangjier/tf-keyworlds)  
运行 python train.py 训练，项目会自动下载音频数据文件，文件超过了2GB，所以，在第一次训练的时候会消耗更多的时间，经过多次迭代后识别的准确率在90%以上。  
